export class taxesDB {
    static data = [
        {
            detail: "GST",
            tax_schedule: "GST18%",
            rate: "18%",
            total_amount: "CLOTHS, SHOES",
        },
        {
            detail: "GST",
            tax_schedule: "GST28%",
            rate: "28%",
            total_amount: "ELECTRONICS",
        },
        {
            detail: "GST",
            tax_schedule: "GST5%",
            rate: "5%",
            total_amount: "GROCERIES",
        },
        // {
        //     detail: "USASTE-PS6N0",
        //     tax_schedule: "USASTCITY-6*",
        //     rate: "40%",
        //     total_amount: "3.20",
        // },
        // {
        //     detail: "USASTE-PS6N0",
        //     tax_schedule: "USASTCITY-6*",
        //     rate: "50%",
        //     total_amount: "4.78",
        // },
        // {
        //     detail: "USASTE-PS6N0",
        //     tax_schedule: "USASTCITY-6*",
        //     rate: "80%",
        //     total_amount: "8.4",
        // }
    ]
}
