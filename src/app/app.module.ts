import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DashboardModule } from './components/dashboard/dashboard.module';
import { SharedModule } from './shared/shared.module';
import { ProductsModule } from './components/products/products.module';
import { SalesModule } from './components/sales/sales.module';
import { CouponsModule } from './components/coupons/coupons.module';
import { PagesModule } from './components/pages/pages.module';
import { MediaModule } from './components/media/media.module';
import { MenusModule } from './components/menus/menus.module';
import { VendorsModule } from './components/vendors/vendors.module';
import { UsersModule } from './components/users/users.module';
import { LocalizationModule } from './components/localization/localization.module';
import { InvoiceModule } from './components/invoice/invoice.module';
import { SettingModule } from './components/setting/setting.module';;
import { ReportsModule } from './components/reports/reports.module';
import { AuthModule } from './components/auth/auth.module';
import { HttpClientModule } from '@angular/common/http';
import { NotifierModule } from 'angular-notifier';




@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    DashboardModule,
    InvoiceModule,
    SettingModule,
    ReportsModule,
    AuthModule,
    SharedModule,
    LocalizationModule,
    ProductsModule,
    SalesModule,
    VendorsModule,
    CouponsModule,
    PagesModule,
    MediaModule,
    MenusModule,
    UsersModule,
    NotifierModule.withConfig({
              position: {
        
              horizontal: {
            
                /**
                 * Defines the horizontal position on the screen
                 * @type {'left' | 'middle' | 'right'}
                 */
                position: 'right',
            
                /**
                 * Defines the horizontal distance to the screen edge (in px)
                 * @type {number} 
                 */
                distance: 12
            
              },
        
          vertical: {
        
            /**
             * Defines the vertical position on the screen
             * @type {'top' | 'bottom'}
             */
            position: 'top',
        
            /**
             * Defines the vertical distance to the screen edge (in px)
             * @type {number} 
             */
            distance: 12,
        
            /**
             * Defines the vertical gap, existing between multiple notifications (in px)
             * @type {number} 
             */
            gap: 10
        
          }
        
        },

        behaviour: {

            /**
             * Defines whether each notification will hide itself automatically after a timeout passes
             * @type {number | false}
             */
            autoHide: 3000,
          
            /**
             * Defines what happens when someone clicks on a notification
             * @type {'hide' | false}
             */
            onClick: false,
          
            /**
             * Defines what happens when someone hovers over a notification
             * @type {'pauseAutoHide' | 'resetAutoHide' | false}
             */
            onMouseover: 'pauseAutoHide',
          
            /**
             * Defines whether the dismiss button is visible or not
             * @type {boolean} 
             */
            showDismissButton: true,
          
            /**
             * Defines whether multiple notification will be stacked, and how high the stack limit is
             * @type {number | false}
             */
            stacking: 1
          
          }
})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
