import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import { AuthenticationService,AlertService} from '../../../_services';

import {Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  public loginForm: FormGroup;  
 
  constructor(private formBuilder: FormBuilder,private router: Router,
    private authservice: AuthenticationService, private alertService: AlertService) {
    this.createLoginForm();        
  }  

  owlcarousel = [
    {
      title: "Welcome to Rapid Baazar",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.",
    },
    {
      title: "Welcome to Rapid Baazar",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.",
    },
    {
      title: "Welcome to Rapid Baazar",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.",
    }
  ]
  owlcarouselOptions = {
    loop: true,
    items: 1,
    dots: true
  };
  
  createLoginForm() {  
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]], 
      Password: ['', Validators.required ] 
    })
  }
 
  login(username: string, Password: string) {     
    this.authservice.login(username, Password)
    .subscribe((data) => {    
     if(data.token!=null){
        this.alertService.notify('success','Logged in successfully.');            
        this.router.navigate(['dashboard/default']);     
     }else{      
       this.alertService.notify('error','Something went wrong.');
     }
     },(err) => {       
        if(err.error=='Unauthorized'){
          this.alertService.notify('error','Please enter valid login details');
        }else{       
          this.alertService.notify('error','Something went wrong.');
        }      
      });
   }  

  ngOnInit() {
  }

}
